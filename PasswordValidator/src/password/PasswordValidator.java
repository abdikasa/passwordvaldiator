package password;

public class PasswordValidator {

	 private static final double MAX_VERSION = 4.0;
	 private String name; //the name of the phone including brand
	
	public PasswordValidator() {
		
	}
	

	/**
	 * Must have a length of 8 and more.
	 * @param pass
	 * @return
	 * @throws NumberFormatException
	 * @throws StringIndexOutOfBoundsException
	 */
	public static boolean validateLength(String pass) throws NumberFormatException, StringIndexOutOfBoundsException 
	{
		if(pass.length() < 8) {
			throw new NumberFormatException("Error: Exceeds Range: You cannot have a password with a length smaller than 8.");
		}
		
		return true;
	}
	
	/**
	 * Must have at least 2 nunbers.
	 * @param pass
	 * @return
	 * @throws NumberFormatException
	 */
	public static boolean validateDigits(String pass) throws NumberFormatException
	{
		/**
		 * Create a string of numbers and check if it is inside our user's password.
		 * we iterate through the loop and return the count.
		 */
		
		String numbers = "0123456789";
		int count = 0;
		
		for(int i = 0; i < pass.length(); i++) {
			if(numbers.indexOf(pass.charAt(i)) >= 0) {
				count+=1;
			}
		}
		
		/**
		 * We will throw an NFE error if count is not greater than 2.
		 */
		
		if(count < 2) {
			throw new NumberFormatException("Password needs more than 2 digits to be valid, actual number of digits: " + count);
		}
		
		return true;
	}
	
	/**
	 * Password should contain uppercase & lowercase letters.
	 * @param pass
	 * @return
	 * @throws NumberFormatException
	 * @throws StringIndexOutOfBoundsException
	 */
	public static boolean validateCase(String pass) throws NumberFormatException, StringIndexOutOfBoundsException 
	{
		boolean countU = false;
		boolean countL = false;
		
		for(int i = 0; i < pass.length(); i++) {
			if(Character.isUpperCase(pass.charAt(i))) {
				countU = true;
			}
			
			if(Character.isLowerCase(pass.charAt(i))) {
				countL = true;
			}
		}
		
		if(countU == false || countL == false) {
			throw new NumberFormatException("Password should contain uppercase & lowercase letters");
		}
		
		
		return true;
	}
	
}
