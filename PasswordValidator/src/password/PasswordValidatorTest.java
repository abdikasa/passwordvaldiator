package password;

/**
 * @author Said Abdikarim 991522345
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	
	/**
	 * Test regular Case: should contain at least 1 upper and lower case character
	 */
	
 	@Test()
	public void testValidateCaseRegular()  {
	boolean length = PasswordValidator.validateCase("SaidAbdikarim");
	assertTrue("Password should contain uppercase & lowercase letters", length == true);
	}
 	
 	/**
	 * Test Exceptional Case: Test with no letters and just digits, should fail ultimately.
	 */
	
	@Test(expected=NumberFormatException.class)
	public void testValidateCaseExceptional()  {
	assertFalse("Password should contain uppercase & lowercase letters", PasswordValidator.validateCase("123"));
	}
	
	/**
	 * Test BO Case: Test with 1 lowercase and no uppercase character. Should not pass.
	 */
	
	@Test(expected=NumberFormatException.class)
	public void testValidateCaseBoundaryOut()  {
	assertFalse("Password should contain uppercase & lowercase letters", PasswordValidator.validateCase("h123"));
}
	
	/**
	 * Test BO Case: Test with 1 lowercase and 1 uppercase character. The bare minimum to succeed. Should pass.
	 */
	
	@Test()
	public void testValidateCaseBoundaryIn()  {
	boolean length = PasswordValidator.validateCase("hT");
	assertTrue("Password should contain uppercase & lowercase letters", length == true);
}
	
	
	
	
	
	
	
	 
	 	@Test()
		public void testValidateLengthRegular()  {
		boolean length = PasswordValidator.validateLength("supermanvsbatman");
		assertTrue("Password should have a length of more than 8 characters", length == true);
	}
	
		@Test(expected=NumberFormatException.class)
		public void testValidateLengthException()  {
		boolean length = PasswordValidator.validateLength("");
		fail("password should have a length of more than 8 characters.");
	}
	
	@Test()
	public void testValidateLengthBoundaryIn()  {
		boolean length = PasswordValidator.validateLength("abdikari");
		assertTrue("password length is greater than or equal to 8", length == true);
	}
	
		@Test(expected=NumberFormatException.class)
	public void testValidateLengthBoundaryOut()  {
		boolean length = PasswordValidator.validateLength("abdikar");
		fail("password length is greater than or equal to 8");
	}
	
//	======================================================================
	
		@Test
	public void testValidateDigitsRegular()  {
		boolean length = PasswordValidator.validateDigits("ramses2456");
		assertTrue("password should contain more than 2 digits", length  == true);
	}
	
	@Test
	public void testValidateDigitsBoundaryIn()  {
		boolean length = PasswordValidator.validateDigits("ramses24");
		assertTrue("password should contain more than 2 digits", length == true);
	}
	
	
	@Test(expected=NumberFormatException.class)
	public void testValidateDigitsException()  {
		boolean length = PasswordValidator.validateDigits("");
		fail("password should contain more than 2 digits");
	}
	
	@Test(expected=NumberFormatException.class)
	public void testValidateDigitsBoundaryOut()  {
		boolean length = PasswordValidator.validateDigits("ramses2");
		fail("password should contain more than 2 ore more digits");
	}
		 

	
}
