package password;

/**
 * A custom Exception that will tell you information
 * about invalid passwords
 * @author Said Abdikarim 991522345
 *
 */

public class PasswordValidatorException extends Exception {

 private static final long serialVersionUID = 1L;

 public PasswordValidatorException() 
 {
  // TODO Auto-generated constructor stub
 }

 public PasswordValidatorException(String message) 
 {
  super( message);
  
 }


}
